from numpy  import *

def rectangle(idnumber,sizea,sizeb,x0,y0,size,totmass,velocity,radius):
        fo = open("Inputs/Rectangle_Run_[" + str(sizea) + "x" + str(sizeb) + "]_velocity_y_" + str(velocity) + "_latticestep_"
         + str(size) + "_radius_"+ str(radius) +"_totalmass_" +str(totmass) +".txt", "wb")
        fo.write("Rectangle_Run_" + str(sizea) + "_" + str(sizeb) + "_velocity_y_" + str(velocity) + "_latticestep_" +
         str(size) + "_radius_"+ str(radius) + "\n")
        fo.write("1\n")
        N=(sizea*sizeb)/float((size*size))
        fo.write(str(N)+"\n")
        for i in arange(y0,y0+sizeb,size):
                for y in arange(x0,x0+sizea,size):
                        fo.write(str(idnumber) + " " + str(totmass/float(N)) + " " + str(float(radius)) + " " + str(y) + " " + str(i) + " 0 0 " + str(velocity) + " 0 0 0 0 0 0 0\n")
                        idnumber = idnumber+1
        fo.write("\n0");
        fo.close();
        return 0

def lineKebab(idnumber,size,nbeads,x0,y0,velocityx,velocityy,totmass):
        idnumber=0
        fo = open("Inputs/LineKebab_Run_size_" + str(size) +  "_Number_of_beads_" + str(nbeads) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_latticestep_" + str((size/float(nbeads))) +"_totalmass_" +str(totmass) +".txt", "wb")
        fo.write("LineKebab_Run_size_" + str(size) + "_Number_of_beads_" + str(nbeads) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_latticestep_" + str(size/float(nbeads)) +"_totalmass_" +str(totmass)+ "\n");
        fo.write("1\n")
        fo.write(str(nbeads)+"\n")
        for i in arange(x0,x0+size,size/float(nbeads)):
                fo.write(str(idnumber) + " " + str(totmass/float(nbeads)) + " " + str(size/(2*float(nbeads))) + " " + str(i) + " 0 0 " + str(velocityx) + " " + str(velocityy) + " 0 0 0 0 0 0 0\n")
                idnumber = idnumber+1

        fo.write("0");
        fo.close()
        return 0
        
def LongRod(idnumber,size,radius,nbeads,x0,y0,velocityx,velocityy,mass):
        idnumber=0
        fo = open("Inputs/LongRod_Run_beadseparation_" + str(size) +  "_Number_of_beads_" + str(nbeads) + "_beads_separation_" + str(radius) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_totalmass_" +str(nbeads*mass) +".txt", "wb")
        fo.write("LongRod_Run_beadseparation_" + str(size) +  "_Number_of_beads_" + str(nbeads) + "_beads_separation_" + str(radius) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_totalmass_" +str(nbeads*mass)+ "\n");
        fo.write("1\n")
        fo.write(str(nbeads)+"\n")
        for i in arange(x0,x0+(nbeads)*size,size):
                fo.write(str(idnumber) + " " + str(mass) + " " + str(radius) + " " + str(i) + " 0 0 " + str(velocityx) + " " + str(velocityy) + " 0 0 0 0 0 0 0\n")
                idnumber = idnumber+1

        fo.write("0");
        fo.close()
        return 0



def Box(idnumber,sizea,sizeb,sizec,radius,x0,y0,z0,velocityx,velocityy,totmass):
    fo = open ("Inputs/Box_" + str(sizea) + "_" + str(sizeb) + "_" + str(sizec) + ".txt", "wb")
    fo.write("Box_" + str(sizea) + "_" + str(sizeb) + "_" + str(sizec) + "\n")
    fo.write("1\n")
    Vdis=radius*radius*radius
    Vbox=sizea*sizeb*sizec
    nbeads=int(Vbox/Vdis)
    mass=(totmass/float(nbeads))
    i=0
    fo.write(str(nbeads)+"\n")
    for x in arange(x0,x0+int(sizea/radius),radius):
        for y in arange(y0,y0+int(sizeb/radius),radius):
            for z in arange(z0,z0+int(sizec/radius),radius):
                fo.write(str(i) + " " + str(mass) + " " + str(radius) + " " + str(x) + " "+  str(y) + " " + str(z) + " " + str(velocityx) + " " + str(velocityy) + " 0 0 0 0 0 0 0\n")     
                i=i+1                
    fo.write("0\n")
    fo.close()
    return 0






def lineKebabwithSpringsX(idnumber,size,nbeads,x0,y0,velocityx,velocityy,totmass):
        fo = open("Inputs/LineKebab_SpringX_Run_size_" + str(size) +  "_Number_of_beads_" + str(nbeads) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_x0_" + str(x0) + "_y0_" + str(y0) + "_latticestep_" +str((size/float(nbeads))) +"_totalmass_" +str(totmass) +".txt", "wb")
        fo.write("LineKebab_Run_size_" + str(size) + "_Number_of_beads_" + str(nbeads) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_latticestep_" + str(size/float(nbeads)) +"_totalmass_" +str(totmass)+ "\n");
        fo.write("1\n")
        fo.write(str(nbeads)+"\n")
        for i in arange(x0,x0+size,size/float(nbeads)):
                fo.write(str(idnumber) + " " + str(totmass/float(nbeads)) + " " + str(size/(2*float(nbeads))) + " " + str(i) + " 0 0 " + str(velocityx) + " " + str(velocityy) + " 0 0 0 0 0 0 0\n")
                idnumber = idnumber+1
        springid=(2*nbeads-3)
        fo.write(str(2*nbeads-3)+ "\n");
	for i in arange(0,nbeads-1,1):
		fo.write(str(i) + " " + str(i) + " " + str(i+1) + " 0 2 " + "1e6 " + str((size/float(nbeads))) + " HarmonicBond" + "\n")
	if (nbeads<=(2*nbeads-3)):
		y=0
		for i in arange(nbeads-1,2*nbeads-3,1):
			fo.write(str(i) + " " + str(y) + " " + str(y+1) + " " + str(y+2) + " 3 1e3 " + str((size/float(nbeads))) + " Bending" + "\n")
			y=y+1
        fo.close()
        return idnumber

def lineKebabwithSpringsY(idnumber,size,nbeads,x0,y0,velocityx,velocityy,totmass):
        fo = open("Inputs/LineKebab_SpringY_Run_size_" + str(size) +  "_Number_of_beads_" + str(nbeads) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_x0_" + str(x0) + "_y0_" + str(y0) + "_latticestep_" + str((size/float(nbeads))) +"_totalmass_" +str(totmass) +".txt", "wb")
        fo.write("LineKebab_Run_size_" + str(size) + "_Number_of_beads_" + str(nbeads) +"_velocity_x_" + str(velocityx) + "_velocity_y_" + str(velocityy) + "_latticestep_" + str(size/float(nbeads)) +"_totalmass_" +str(totmass)+ "\n");
        fo.write("1\n")
        fo.write(str(nbeads)+"\n")
        for i in arange(x0,x0+size,size/float(nbeads)):
                fo.write(str(idnumber) + " " + str(totmass/float(nbeads)) + " " + str(size/(2*float(nbeads))) + " 0 " + str(i) + " 0 " + str(velocityx) + " " + str(velocityy) + " 0 0 0 0 0 0 0\n")
                idnumber = idnumber+1

        fo.write(str(2*nbeads-3)+ "\n");
        for i in arange(0,nbeads-1,1):
                fo.write(str(i) + " " + str(i) + " " + str(i+1) + " 0 2 " + "1 " + str((size/float(nbeads))) + " HarmonicBond" + "\n")
        if (nbeads<=(2*nbeads-3)):
                y=0
                for i in arange(nbeads-1,2*nbeads-3,1):
                        fo.write(str(i) + " " + str(y) + " " + str(y+1) + " " + str(y+2) + " 3 0.1 " + str((size/float(nbeads))) + " Bending" + "\n")
                        y=y+1
        fo.close()
        return idnumber


def CylinderHollow(a,radius,nbeads,vx,vy,x0,y0,z0,totmass):
        
        def CircleN(idnumber,r,N,vx,vy,x0,y0,z0):
                ang=2.0*pi/N
                #dr=(r*r*sin(ang))/(2.0*r+r*sin(ang/2.0))
                dr=(sin(ang/2.0)*cos(ang/2.0)*r)/(1+sin(ang/2.0))
                h=r*cos(ang/2.0)
                for y in arange(0,N,1):
                        xi=x0+cos(y*ang)*(h-dr)
                        yi=y0
                        zi=z0+sin(y*ang)*(h-dr)
                        fo.write(str(idnumber) + " " + str(totmass/float(totalN)) + " " + str(dr) + " " + str(xi) + " " + str(yi) + " " + str(zi) + " " + str(vx) + " " + str(vy) + " 0 0 0 0 0 0 0 \n" )
                        idnumber=idnumber+1
                return idnumber
                
        
        angle=2.0*pi/nbeads;
        d=(sin(angle/2.0)*cos(angle/2.0)*radius)/(1+sin(angle/2.0))
        totalN=0
        for i in arange(0,a,2.0*d):
                totalN=totalN+nbeads
                
        fo = open("Inputs/Cylinder_Hollow_Run_length_" + str(a) +  "_radius_" + str(radius) +"_vx_" + str(vx) + "_vy_" + str(vy) + "_latticestep_" + str(d) +"_totalmass_" +str(totmass) +".txt", "wb")
        fo.write("Cylinder_Hollow_Run_length_" + str(a) +  "_radius_" + str(radius) +"_vx_" + str(vx) + "_vy_" + str(vy) + "_latticestep_" + str(d) +"_totalmass_" +str(totmass) + "\n");
        fo.write("1\n")
        fo.write(str(totalN)+"\n")
        
        idnumber=0
        for i in arange(0,a,2.0*d):
                idnumber=CircleN(idnumber,radius,nbeads,vx,vy,x0,i,z0)
                
                
        fo.write("0");        
        fo.close()        
        return idnumber




#-------------------
def CylinderFilled(a,radius,nbeads,vx,vy,x0,y0,z0,totmass):
        
        def CircleN(idnumber,r,N,vx,vy,x0,y0,z0):
                ang=2.0*pi/N
                #dr=(r*r*sin(ang))/(2.0*r+r*sin(ang/2.0))
                dr=(sin(ang/2.0)*cos(ang/2.0)*r)/(1+sin(ang/2.0))
                h=r*cos(ang/2.0)
                for y in arange(0,N,1):
                        xi=x0+cos(y*ang)*(h-dr)
                        yi=y0
                        zi=z0+sin(y*ang)*(h-dr)
                        fo.write(str(idnumber) + " " + str(totmass/float(totalN)) + " " + str(dr) + " " + str(xi) + " " + str(yi) + " " + str(zi) + " " + str(vx) + " " + str(vy) + " 0 0 0 0 0 0 0 \n" )
                        idnumber=idnumber+1
                return idnumber
                
        def Num(r,dr):
                coeff = [1.0, -2.0*d,d*d-r*r , 2.0*r*r*d]
                ro = roots(coeff)
                ror=[]
                for i in arange(0,len(ro),1):
                        if (ro[i]==ro[i].real):
                                ror[len(ror):] = [ro[i].real]
                
                h=max(ror)
                ang=2.0*arccos(h/r)
                n=floor(2.0*pi/ang)
                ang=(2.0*pi/n);
                return n
        
        
        
        def CircleD(idnumber,r,dr,vx,vy,x0,y0,z0):
                coeff = [1.0, -2.0*d,d*d-r*r , 2.0*r*r*d]
                ro = roots(coeff)
                ror=[]
                for i in arange(0,len(ro),1):
                        if (ro[i]==ro[i].real):
                                ror[len(ror):] = [ro[i].real]
                
                h=max(ror)
                ang=2.0*arccos(h/r)
                n=floor(2.0*pi/ang)
                ang=(2.0*pi/n);        
                h=r*cos(ang/2.0)
                for y in arange(0,n,1.0):
                        xi=x0+cos(y*ang)*(h-dr)
                        yi=y0
                        zi=z0+sin(y*ang)*(h-dr)
                        fo.write(str(idnumber) + " " + str(totmass/float(totalN)) + " " + str(dr) + " " + str(xi) + " " + str(yi) + " " + str(zi) + " " + str(vx) + " " + str(vy) + " 0 0 0 0 0 0 0 \n" )
                        idnumber=idnumber+1
                return idnumber
                        
        angle=2.0*pi/nbeads;
        d=(sin(angle/2.0)*cos(angle/2.0)*radius)/(1.0+sin(angle/2.0))
        totalN=0
        for i in arange(0,a,2.0*d):
                totalN=totalN+nbeads
        for y in arange(radius-2.0*d,3.0*d,-2.0*d):     
                for i in arange(0,a,2.0*d):        
                        totalN=totalN+Num(y,d)
                
        fo = open("Inputs/Cylinder_Filled_Run_length_" + str(a) +  "_radius_" + str(radius) +"_vx_" + str(vx) + "_vy_" + str(vy) + "_latticestep_" + str(d) +"_totalmass_" +str(totmass) +".txt", "wb")
        fo.write("Cylinder_Fillded_Run_length_" + str(a) +  "_radius_" + str(radius) +"_vx_" + str(vx) + "_vy_" + str(vy) + "_latticestep_" + str(d) +"_totalmass_" +str(totmass) + "\n");
        fo.write("1\n")
        fo.write(str(totalN)+"\n")
        
        idnumber=0
        for i in arange(0,a,2.0*d):
                idnumber=CircleN(idnumber,radius,nbeads,vx,vy,x0,i,z0)
        for y in arange(radius-2.0*d,3.0*d,-2.0*d):          
                for i in arange(0,a,2.0*d):
                        idnumber=CircleD(idnumber,y,d,vx,vy,x0,i,z0)
                
        fo.write("0");        
        fo.close()        
        return idnumber







#def square(iden,a,x,y):
#return

#def circle(iden,r,x,y):
#return

Nsquare=0
Nrectange=1
Ncircle=0
Totalmass=1.0; # 1kg



#for m in xrange(2,100,1):
#       LongRod(0,10,1,m,0,0,1,0,1)     

#for m in xrange(2,100,1):
#       LongRod(0,10,1,m,0,0,0,1,1)     


#for m in xrange(2,200,1):
#       lineKebab(0,100,m,0,0,1,0,Totalmass)    

#for m in xrange(2,300,1):
 #     lineKebabwithSpringsY(0,50,m,0,0,0,0,0.00015)    
 #    lineKebabwithSpringsX(0,50,m,0,0,0,0,0.00015) 

#ide=lineKebabwithSpringsX(0,50,50,0,0,0,0,0.00015)
#lineKebabwithSpringsX(ide,50,50,0,10,10,0,0.00015)

#lineKebabwithSpringsX(0,30,30,0,0,0,0,9)
#lineKebabwithSpringsX(0,50,50,0,0,0,0,50*3)


"""
rectangle(0,400,100,0,0,float(100),Totalmass,1,100/float(2));
rectangle(0,400,100,0,0,float(50),Totalmass,1,50/float(2));
rectangle(0,400,100,0,0,float(25),Totalmass,1,25/float(2));
rectangle(0,400,100,0,0,float(12.5),Totalmass,1,12.5/float(2));
rectangle(0,400,100,0,0,float(6.25),Totalmass,1,6.25/float(2));
rectangle(0,400,100,0,0,float(3.125),Totalmass,1,3.125/float(2));
#rectangle(0,400,100,0,0,1,Totalmass,1,1);"""

Box(0,15,19,4,1,0,0,0,1,0,593)
