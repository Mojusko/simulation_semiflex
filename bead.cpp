#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <Dense>
#include <Eigen>
#include "physics_constants.h"
#include "bead.h"
#include "helper.h"

using namespace Eigen;
using namespace std;
extern int N;
extern bead * beads[10000];
extern int filament[10000][10000];
//#define Regularized_Stokelets 1
//#define RotnePrager 1

//Constructor for Class Bead
bead::bead(int idnumber, double mass, double size2, const Vector3f& position) {

	r=position;
	id=idnumber;
	m=mass;
	size=size2;
	v=Vector3f::Zero();
	F=Vector3f::Zero();
	u=Vector3f::Zero();

}

//Destructor for Class bead

bead::~bead(){}


//Flow Field Calculation

void bead::calculate_flow_field_from_drag(double norm) {


Vector3f old;

old = this->u;


(this->u) << 0.0,0.0,0.0;

	Matrix3f O,I;
	I=Matrix3f::Identity();
	double k=1/(8*M_PI*Miu);
	double n,C;

	//Oseen Tensor

for (int i=0;i<N;i++) {

	Vector3f R,M,Rhat,A,B;
	R=(this->r)-(beads[i]->r); // R.. separation between two particles
	n=R.norm();
	Rhat=R/n;
		if (n>epsilon) {
			

			#ifdef Regularized_Stokelets 
				double E=beads[i]->size;
				M=-1*(beads[i]->Drag);
				C=R.dot(M);
				A=k*(sqr(n)+2*sqr(E))/(sqrt(cube(sqr(n)+sqr(E))))*M;
				B=k*(R*C)/(sqrt(cube(sqr(n)+sqr(E))));
				this->u=(this->u)+(A+B);
			#endif

			#ifdef RotnePrager
					double E=beads[i]->size;
					M=-1*(beads[i]->Drag);
					C=Rhat.dot(M)
					double c=1/(12*M_PI*Miu);
					this->u=(this->u)+k/n*(M+Rhat*C)+((c*sqr(E))/cube(n))*(M-3*Rhat*C);
			#else
					//O=(k/(R.norm()))*(I+((R*(R.transpose()))/(sqr(R.norm())))); //Oseen Tensor
					M=-1*(beads[i]->Drag);
					//temp[id]->u=(temp[id]->u)+O*M;
					C=Rhat.dot(M);
					this->u=(this->u)+(k/n)*((M)+Rhat*C);
			#endif
		}
	}
		this->u=old+0.001*(this->u-old);

        for (int id=0;id<N;id++) {    //calculate drag on each particle
                        beads[id]->compute_drag();
        }


}






void bead::calculate_flow_field_from_force(double norm) {


(this->u) << 0.0,0.0,0.0;

	Matrix3f O,I;
	I=Matrix3f::Identity();
	double k=1.0/(8.0*M_PI*Miu);
	double n,C;


for (int i=0;i<N;i++) {

        Vector3f R,M,Rhat,A,B;
        R=(this->r)-(beads[i]->r); // R.. separation between two particles
        n=R.norm();
        Rhat=R/n;
        
                if (n>epsilon) {
                        

        #ifdef Regularized_Stokelets 
                                M=(beads[i]->F);
				double E=beads[i]->size;
                                C=R.dot(M);
                                A=k*(sqr(n)+2*sqr(E))/(sqrt(cube(sqr(n)+sqr(E))))*M;
                                B=k*(R*C)/(sqrt(cube(sqr(n)+sqr(E))));
                        
                                
                                this->u=(this->u)+(A+B);
                        #endif

                        #ifdef RotnePrager
					double c=1.0/(12.0*M_PI*Miu);
					double E=beads[i]->size;
                                        M=(beads[i]->F);
                                        C=Rhat.dot(M);
                                        this->u=(this->u)+k/n*(M+Rhat*C)+((c*sqr(E))/cube(n))*(M-3*Rhat*C);
                        #else //Oseen Regular
                                        M=(beads[i]->F);
                                        C=Rhat.dot(M);
                                        this->u=(this->u)+(k/n)*(M);//+(k/n)*(Rhat*C);
                                        
                        #endif
                }
        }

}







void bead::compute_drag() { 

 	Vector3f Force=Vector3f::Zero();
	Force = -Miu*6*M_PI*(this->size)*((this->v)-(this->u));  // F=F-miu*r*(v-u)
	this->Drag=Force; 

}

void bead::compute_flow_force(double norm) {
	Vector3f Force=Vector3f::Zero();
	Force = (this->F)-Miu*6*M_PI*(this->size)*((this->v)-(this->u));  // F=F-miu*r*(v-u)
	this->F=Force; 

}
