#include <Dense>
#include <Eigen>

using namespace Eigen;


//Class bead
class bead {
	
    public: 
      Vector3f r;
      Vector3f v;
      Vector3f F;
      Vector3f Drag;
      Vector3f u;
      double m,size;
      int id;
     
	bead(int, double, double, const Vector3f &);
	~bead();
	void calculate_flow_field_from_drag(double);
	void calculate_flow_field_from_force(double);
	void compute_drag();
	void compute_flow_force(double); 
	
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
      };

