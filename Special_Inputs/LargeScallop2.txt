LargeScallop_Two
2
10
0       3.00E+000       0.5     0       0       0       0       0       0       0       0       0       0       0       0
1       3.00E+000       0.5     1       0       0       0       0       0       0       0       0       0       0       0
2       3.00E+000       0.5     2       0       0       0       0       0       0       0       0       0       0       0
3       3.00E+000       0.5     3       0       0       0       0       0       0       0       0       0       0       0
4       3.00E+000       0.5     4       0       0       0       0       0       0       0       0       0       0       0
5       3.00E+000       0.5     5       0       0       0       0       0       0       0       0       0       0       0
6       3.00E+000       0.5     6       0       0       0       0       0       0       0       0       0       0       0
7       3.00E+000       0.5     7       0       0       0       0       0       0       0       0       0       0       0
8       3.00E+000       0.5     8       0       0       0       0       0       0       0       0       0       0       0
9       3.00E+000       0.5     9       0       0       0       0       0       0       0       0       0       0       0
10
10       3.00E+000       0.5     0       3       0       0       0       0       0       0       0       0       0       0
11       3.00E+000       0.5     1       3       0       0       0       0       0       0       0       0       0       0
12       3.00E+000       0.5     2       3       0       0       0       0       0       0       0       0       0       0
13       3.00E+000       0.5     3       3       0       0       0       0       0       0       0       0       0       0
14       3.00E+000       0.5     4       3       0       0       0       0       0       0       0       0       0       0
15       3.00E+000       0.5     5       3       0       0       0       0       0       0       0       0       0       0
16       3.00E+000       0.5     6       3       0       0       0       0       0       0       0       0       0       0
17       3.00E+000       0.5     7       3       0       0       0       0       0       0       0       0       0       0
18       3.00E+000       0.5     8       3       0       0       0       0       0       0       0       0       0       0
19       3.00E+000       0.5     9       3       0       0       0       0       0       0       0       0       0       0
34
0       0       1       0       2       1        1       HarmonicBond
1       1       2       0       2       1        1       HarmonicBond
2       2       3       0       2       1        1       HarmonicBond
3       3       4       0       2       1        1       HarmonicBond
4       4       5       0       2       1        1       HarmonicBond
5       5       6       0       2       1        1       HarmonicBond
6       6       7       0       2       1        1       HarmonicBond
7       7       8       0       2       1        1       HarmonicBond
8       8       9       0       2       1        1       HarmonicBond
9       10       11     0       2       1        1       HarmonicBond
10      11       12     0       2       1        1       HarmonicBond
11      12       13     0       2       1        1       HarmonicBond
12      13       14     0       2       1        1       HarmonicBond
13      14       15       0       2       1        1       HarmonicBond
14      15       16       0       2       1        1       HarmonicBond
15      16       17       0       2       1        1       HarmonicBond
16      17       18       0       2       1        1       HarmonicBond
17      18       19       0       2       1        1       HarmonicBond



18       0       1       2       3       1        1       Bending
19      1       2       3       3       1        1       Bending
20      2       3       4       3       1        1       Bending
21      3       4       5       3       1        1       Bending
22      4       5       6       3       1        1       Bending
23      5       6       7       3       1        1       Bending
24      6       7       8       3       1        1       Bending
25      7       8       9       3       1        1       Bending

26       10      11       12       3       1        1       Bending
27      11       12       13       3       1        1       Bending
28      12       13       14       3       1        1       Bending
29      13       14       15       3       1        1       Bending
30      14       15       16       3       1        1       Bending
31      15       16       17       3       1        1       Bending
32      16       17       18       3       1        1       Bending
33      17       18       19       3       1        1       Bending