#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <Dense>
#include <Eigen>

#include "bond.h"

using namespace Eigen;
using namespace std;
//Constructor for Class Bond
bond::bond(int idnumber_c, int id1_c, int id2_c, int typebond_c, double strength_c, std::string bondname_c)
{
	id=idnumber_c;
	id1=id1_c;
	id2=id2_c;
	nature=typebond_c;
	strength=strength_c;
	name_bond=bondname_c;	
}

//Destructor for Class bead

bond::~bond(){}


Vector3f bond::planenorm(const Vector3f& R1,const Vector3f& R2,const Vector3f& R3) {

Vector3f m,n,no;
no=Vector3f::Zero();
/*cout << "R1 " << R1.transpose() << "\n"; 
cout << "R2 " << R2.transpose() << "\n";
cout << "R3 " << R3.transpose() << "\n";*/
m=R2-R3;
n=R2-R1;
//cout << m.transpose() << "\n";
//cout << n.transpose() << "\n";
no=m.cross(n);
//cout << no.transpose() << "\n";
no=no/(no.norm());


return no;


}
