#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>

double distancebetweenfilaments(int,int);
double sqr(double );
double cube(double );
double max(double ,double );
double min(double ,double );   
double filamentbulgeY(int);
double TotalVelocityOnFilament(int );
double TotalForceOnFilament(int );    
double TotalMassOnFilament(int );   
std::string double_to_string(double);
