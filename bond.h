#include <Dense>
#include <Eigen>
using namespace Eigen;


//Class bond
class bond {

    public: 
     int id1,id2,id3,id,nature;
     double strength,x0;
     std::string name_bond;
     bond(int, int, int, int, double,std::string);
     ~bond();
     Vector3f planenorm(const Vector3f& ,const Vector3f& ,const Vector3f& ) ;
    
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW; //DUE TO EIGEN LIBRARY
  };

