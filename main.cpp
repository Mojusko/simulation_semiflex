#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <time.h>
#include <math.h>
#include <Dense>
#include <Eigen>
#include "bead.h"
#include "bond.h"
#include "IO_handling.h"
#include "helper.h"
#include "physics_constants.h"

using namespace Eigen;
using namespace std;



//--------GLOBAL CONSTANTS---SIMULATION
int    TIME = 100; // time of steps
double dt=0.01;        // step size
double t;
string name="";  // Default name of Simulaton
string in="input.txt"; // Default Input
int mode=1;       // Default Debug Mode
int storetime= 1; // Steps of Store Time



//Physics model of simulation
//#define iterative_flow_field 1
#define total_force 1
#define ACTIVEFORCES

//adaptive integration constants
double AVVERR=1e-4;
#define AVVDTS 0.9
#define AVVDTMAX 1
#define AVVDTMIN 1e-20

//--------GLOBAL CONSTANTS--- PHYSICS
// in file physics_constants.h
//--------GLOBAL CONSTANTS--- MATH
Vector3f zerovector=Vector3f::Zero();

//-------GLOBAL VARIABLES
	int N=0; //NUMBER OF BEADS
	int L=0; //NUMBER OF FILAMENTS
	int Nbonds=0;
	bead * beads[10000]; 
	bond * bonds[10000];
    int filament[10000][10000];
	double oldaveragevelocity=0;
	double totalforce=0;
	double totalmass=0;
    bool ActiveOn=true;
	bool ExternalForceOn=true;
    double length;
//-------------------------------------------
//-----------------------------------------------








//-----------------------COMPUTATIONAL FUNCTIONS

//------------------------------ BEAD INTERACTIONS-----------------------------------
void compute_interactions(double interaction_length) {


	if (ExternalForceOn=true) {
	//----EXTERNAL FORCE
        	Vector3f Force=Vector3f::Zero();
		for (int y=0;y<N;y++) {
			Force << 0.0, -1.0*(gravity*beads[y]->m), 0.0;
			beads[y]->F=Force; 
		}
	}
	
	//-----INTERNAL FORCES


	//-------BONDS EVALUATION----------
	double k,norm,r0,rmax;
	Vector3f vectorR=zerovector;
	for (int i=0;i<Nbonds;i++) {
		switch (bonds[i]->nature) 
		{
			case 1:  {	//Lennard-Jones Potential Forces
				k=bonds[i]->strength;
				vectorR=(beads[bonds[i]->id1]->r)-(beads[bonds[i]->id2]->r);
				norm=vectorR.norm();
                r0=bonds[i]->x0;
                double C6=pow(r0,1/6);
				beads[bonds[i]->id1]->F+=(vectorR/norm)*((12*k/pow(norm,13))-((12*C6)*k/pow(norm,7)));
				beads[bonds[i]->id2]->F+=-(vectorR/norm)*((12*k/pow(norm,13))-((12*C6)*k/pow(norm,7)));
		      }
            break;	
			case 2:	{	//Harmonic Bonding
				k=bonds[i]->strength;
				vectorR=(beads[bonds[i]->id1]->r)-(beads[bonds[i]->id2]->r);
				norm=vectorR.norm();
				r0=bonds[i]->x0;
				beads[bonds[i]->id1]->F+=-k*(norm-r0)*(vectorR/norm);
				beads[bonds[i]->id2]->F+=k*(norm-r0)*(vectorR/norm);
				}
			break; 
			case 3:	{	//Harmonic Bending
				k=bonds[i]->strength;	
				Vector3f vector1 =(beads[bonds[i]->id1]->r);
				Vector3f vector2 =(beads[bonds[i]->id2]->r);
				Vector3f vector3 =(beads[bonds[i]->id3]->r);
				beads[bonds[i]->id1]->F+=-k*2*(vector1+vector3-2*vector2);
				beads[bonds[i]->id2]->F+=k*4*(vector1+vector3-2*vector2);
				beads[bonds[i]->id3]->F+=-k*2*(vector1+vector3-2*vector2);
				}
			break;

            case 4: {       //FENE Bonding
                k=bonds[i]->strength;   
                r0=bonds[i]->x0;
                vectorR=(beads[bonds[i]->id1]->r)-(beads[bonds[i]->id2]->r);
                norm=vectorR.norm();
                rmax=0.1*r0;
                beads[bonds[i]->id1]->F+=(vectorR/norm)*k*((2*(norm-r0)*(sqr(norm)-2*r0*norm+2*rmax))/(sqr(norm-2*r0)*cube(norm)));
                beads[bonds[i]->id2]->F+=-(vectorR/norm)*k*((2*(norm-r0)*(sqr(norm)-2*r0*norm+2*rmax))/(sqr(norm-2*r0)*cube(norm)));
                }
            break;





		}

             
	}
        //--------------ACTIVE FORCE -----------------
        
        
        #ifdef ACTIVEFORCES
                if (ActiveOn==true) {
                        double T0,ku,omega,F1,F2,T;
                        T0=-1000000.;
                        ku=1./length*2.0*M_PI;
                        omega=1;
                        Vector3f R1,R2,E1,E2,normal;
                
                        for (int i=0;i<L;i++) {
                                for (int y=2;y<filament[i][0];y++) { //starting from 1st as 0th is #of beads in fillament 
                                        T=T0*sin(y*ku+2.0*M_PI*omega*t*dt);
                                        R1=(beads[filament[i][y]]->r)-(beads[filament[i][y-1]]->r);
                                        R2=(beads[filament[i][y]]->r)-(beads[filament[i][y+1]]->r);
                                        F1=2.0*T/R1.norm();
                                        F2=2.0*T/R2.norm();
                                
                                        //normal=bonds[0]->planenorm(beads[filament[i][y-1]]->r,beads[filament[i][y]]->r,beads[filament[i][y+1]]->r);
                                        normal << 0.0,0.0,1.0; 
                                        E1=normal.cross(R1);
                                        E2=normal.cross(R2);
                                        beads[filament[i][y-1]]->F+=-F1*E1/E1.norm();
                                        beads[filament[i][y]]->F+=F1*E1/E1.norm();
                                        beads[filament[i][y]]->F+=-F2*E2/E2.norm();
                                        beads[filament[i][y+1]]->F+=F2*E2/E2.norm();
                                        if (mode==5) { cout << "t: " << t*dt << " " <<  sin(y*ku+2.0*M_PI*omega*t*dt) << "\n";
                                                        
                                                }
                                }
                        }
                }
        #endif
        
                

        //--------------------------------------------
	

}
//-----------------------COMPUTE FLOW FIELD ----- Iterative Method-------------------
void compute_whole_flow_field(double norm) {

	#ifdef iterative_flow_field
                Vector3f oldu[100000];
                double difference=0;
                int counter = 0;
                int counter2= 0;
                bool done=false;
                double field;
                double fraction = 0;
                do {
                        
                        counter++;
                        for (int id=0;id<N;id++) {    //calculate drag
                                beads[id]->compute_drag();
                        }

                        difference=0;
                        for (int id=0;id<N;id++) { // Looping over beads
                                oldu[id]=beads[id]->u;
                                counter2=0;
                                beads[id]->calculate_flow_field_from_drag(norm);
                                difference=difference+abs((oldu[id]-beads[id]->u).norm());
                                if (mode==4) { cout << "P" << id << " Diff " <<difference << " Field " <<  beads[id]->u.transpose() << " Drag " << beads[id]->Drag.transpose()<<  " Velocity "<< beads[id]->v.transpose() << "\n" ;}
                        }       
                } 
                while (difference>0.0001*N || counter<10 || done==true);
                if (mode==4) {cout << "Iteration ended\n";}

        #else

                 for (int id=0;id<N;id++) {    //calculate drag
                                beads[id]->compute_drag();
                        }

                for (int id=0;id<N;id++) {
                        beads[id]->calculate_flow_field_from_drag(norm);
                        if (mode==4) { cout << "P" << id << " Field " <<  beads[id]->u.transpose() << " Drag " << beads[id]->Drag.transpose()<< "\n";}
                }
        #endif
}	


//------------------------ FLOW_FORCES
void compute_whole_flow_forces(double norm) { 


	for (int y=0;y<N;y++) {
        		beads[y]->compute_flow_force(0);    
        }

}
//--------------------------------------------------------------------------------------
//-----------------------------EXTRA FUNCTIONS------------------------------------------

bool VelocitySaturationCheck() {
        
        Vector3f TotalForce = Vector3f::Zero();
        Vector3f TotalVelocity = Vector3f::Zero();
	double averagevelocity=0;
	for (int i=0;i<N;i++) {
		TotalVelocity+=(beads[i]->v);
	}       
        averagevelocity=TotalVelocity.norm();


	if ((abs(averagevelocity-oldaveragevelocity)<0.00000000000001*N)) {
		totalforce=0;
		totalmass=0;
		for(int i=0;i<N;i++) {
			TotalForce=TotalForce+(beads[i]->F);
			totalmass+=beads[i]->m;
		}													
                
                
                totalforce=TotalForce.norm();					
		return true;
	}
	else {
		totalforce=0;
		totalmass=0;
        
		for(int i=0;i<N;i++) {
			TotalForce=TotalForce+(beads[i]->F);
			totalmass+=beads[i]->m;
		}	
                totalforce=TotalForce.norm();
		oldaveragevelocity=averagevelocity; 
		return false;
	}
}


bool ForceSaturationCheck() {
        
        Vector3f TotalForce = Vector3f::Zero();
        Vector3f TotalVelocity = Vector3f::Zero();
        double averagevelocity=0;
        totalforce=0;
        totalmass=0;

        for (int i=0;i<N;i++) {
                TotalVelocity+=(beads[i]->v);
                TotalForce+=(beads[i]->F);
                totalmass+=beads[i]->m;
        }       
        averagevelocity=TotalVelocity.norm();
        totalforce=TotalForce.norm();



        if (totalforce<0.000001*N) {
                               
                return true;
        }
        else {
                oldaveragevelocity=averagevelocity; 
                return false;
        }
}



//-----------------------------------------------------------------------------------------------


int main(int argc, char *argv[])
{
	in=argv[1]; // input file name
	mode=atoi(argv[2]);
	Initialize_IO(in); // initiallize input of information + output settings
	


//----------MAIN LOOP-----------------VELOCITY-VERLE-ALGORITHM--------    
        t=1.0;
        while (t<=TIME) {

              
                //-------------------STEP TIMER CONTROL 
                		
               /* if (t>TIME/4)
                {
                        ActiveOn=false;
			
                        if (t>TIME*3/4) { ActiveOn=true;}
 
                }
                */
                //------------------------------------------Integration

                double mdt=dt;
                double t0=0.0;
                double t1=dt;
                double tmperror,maxerror,tmpmdt;
                while (t0<t1) {
                                //----------------------------ADAPTIVE TIME STEP
                                //-------------------------------------------------------------------------
                                // Euler error exstimation
                                maxerror=0;
                                for (int y=0;y<N;y++) {
                                        tmperror=(sqrt((beads[y]->F).dot(beads[y]->F))*sqr(mdt))/((beads[y]->m));
                                        if(tmperror>maxerror) { maxerror=tmperror;}        
                                }
                                 
                        
                                tmpmdt=AVVDTS*sqrt(AVVERR/(maxerror/sqr(mdt)));
                                if (maxerror>AVVERR  && tmpmdt<mdt){
                                        mdt=max(tmpmdt,AVVDTMIN);
                                }
                                if (mode==6) { cout << t*dt << " " <<  dt << " Current Error: " <<  maxerror << " tmpdt: " << tmpmdt<<  " Current mdt: " << mdt << "\n"; }

                                //------------------------------------------------------------------------
                
                                //----------------------VELOCITY VERLE
                        
		                //UPDATE OF VELOCITY v.1
		                for (int y=0;y<N;y++) {
			                (beads[y]->v)=(beads[y]->v)+(0.5*mdt*(beads[y]->F))/(beads[y]->m); // On one bead: velocity+=Force/m*1/2*dt
		                }
		                //------------------
                
                
		                //UPDATE OF POSITION
		                for (int y=0;y<N;y++) {
			                beads[y]->r=(beads[y]->r)+(beads[y]->v)*mdt;  // On one bead: position+=velocity*dt
		                }
		                //--------------
                
                
                
	                
                
		                //---COMPUTE FORCES ACTING ON SOLID BODIES
                
                            compute_interactions(0); 
                            compute_whole_flow_field(0);
	                        compute_whole_flow_forces(0);   
                        //--------------------  
                
                
		                //UPDATE OF VELOCITY v.2
		                for (int y=0;y<N;y++) {
			                beads[y]->v=(beads[y]->v)+(0.5*mdt*(beads[y]->F))/(beads[y]->m);// On one bead: velocity+=Force/m*1/2*dt
		                }
		                
                                //-------------------------------------------------------

                                if (t0+mdt > t1) { mdt=t1-t0; }
                                t0+=mdt;

                                // If error < 50% than wished error (I follow GSL adptive time step driver)
                                if (maxerror<AVVERR/2. && tmpmdt/mdt>1./5.  ){
                                        mdt=min(min(tmpmdt,mdt*5.0),AVVDTMAX);
                                }

                
		

                }
                
                //---------------------------Storage/Output of Information
                if ((int)(t) % storetime==0) { 
                        StoreXYZ(name); 
                        //StoreDebug(name,t*dt);
                        Debugout(name,t*dt);
                }


                //---------------------Field Data Storage
              /*if (distancebetweenfilaments(0,1)<beads[0]->size*2) {
                    StorePosition(name);
                    StoreField(name);
                
                    exit(0);

                }*/
             
		
		//----------------------Saturation Check-------------------------------
		double end=false;
		end=VelocitySaturationCheck();
        end=ForceSaturationCheck();
                
        end=false;
		
        if (end==true) {
		       cout << N << " Average Velocity: " << oldaveragevelocity/N << " TotalMass: " << totalmass  << " TotalFoce: " << totalforce << " Gamma: " << totalmass*gravity/(oldaveragevelocity/N) << "\n";  
	           cout << " Distance Between Filaments 0-1: " << distancebetweenfilaments(0,1) << "\n";	
        	break;    
		} 
                
         	if (mode==3 || mode==8 || mode==4) { cout << t*dt << " nanos " << "Average Velocity: " << oldaveragevelocity/N << " microm TotalMass: " << totalmass  << " pg TotalFoce: " << totalforce << " nanoN Gamma: " << totalforce/(oldaveragevelocity/N) << "\n"; 
             
                }

                //----------------------------------------------------------------------
                
	        t=t+1.0;
        }
	      
//-----------------------------------------------------      
    
    
	return 0;
}
