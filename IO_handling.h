#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <Dense>
#include <Eigen>

using namespace std;

void input(const std::string&);
void Initialize_IO(const std::string&);
void StoreDebug(const std::string&, double);
void Debugout(const std::string&, double);
void StoreXYZ(const std::string&) ;
void StorePosition(const std::string&) ;
void StoreField(const std::string&) ;
void StoreDebugInfo(const std::string&, double, int);


