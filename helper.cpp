#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <Dense>
#include <Eigen>
#include "helper.h"
#include "bead.h"
#include "bond.h"

using namespace Eigen;
using namespace std;

extern bead * beads[1000]; 
extern bond * bonds[1000];
extern int filament[1000][1000];
extern int N;
extern int L;
extern int Nbonds;
extern string name;
extern int TIME;
extern int mode;
extern double dt;
extern double length;

double distancebetweenfilaments(int a,int b)
{
double totalmass1,totalmass2;
Vector3f R1,R2,R;
R1=Vector3f::Zero();
totalmass1=0;

for (int i=0;i<filament[a][0];i++) {
        R1=(beads[filament[a][i+1]]->m)*(beads[filament[a][i+1]]->r)+R1;
        totalmass1+=beads[filament[a][i+1]]->m;
}
 
R2=Vector3f::Zero();
totalmass2=0;
       
for (int i=0;i<filament[b][0];i++) {
        R2=(beads[filament[b][i+1]]->m)*(beads[filament[b][i+1]]->r)+R2;
        totalmass2+=beads[filament[b][i+1]]->m;
}

R=((1.0/totalmass1)*R1)-((1.0/totalmass2)*R2);
return R.norm();



}

double filamentbulgeY(int a) {

	double ymax,ymin;
	ymax=beads[0]->r(1);
	ymin=beads[0]->r(1);

	for (int i=0;i<filament[a][0];i++) {

		if (filament[a][i+1]>ymax) { ymax=beads[i]->r(1);}
		if (filament[a][i+1]>ymin) { ymin=beads[i]->r(1);}


	}

	return abs(ymax-ymin);

}

double TotalVelocityOnFilament(int a) {

Vector3f TotalVelocity=Vector3f::Zero();

for (int i=0;i<filament[a][0];i++) {

		TotalVelocity+=beads[i+1]->v;
	}
return TotalVelocity.norm();



}


double TotalForceOnFilament(int a) {

Vector3f TotalForce=Vector3f::Zero();

for (int i=0;i<filament[a][0];i++) {

		TotalForce+=beads[i+1]->F;
	}
return TotalForce.norm();



}


double TotalMassOnFilament(int a) {

double totalmass=0;

for (int i=0;i<filament[a][0];i++) {

		totalmass+=beads[i+1]->m;
	}
return totalmass;



}



double sqr(double x) {return x*x;};


double cube(double x) {return x*x*x;};


double max(double x,double y) {if (x>y) { return x;} else {return y;}};


double min(double x,double y) {if (x<y) { return x;} else {return y;}};  



std::string double_to_string(double value) {
    stringstream sstr;
    sstr << value;
    return sstr.str();
}

      
