#include <cstdlib>
#include <fstream>
#include <iostream>
#include <time.h>
#include <math.h>
#include <Dense>
#include <Eigen>
#include "bond.h"
#include "bead.h"
#include "IO_handling.h"
#include "helper.h"
#include "physics_constants.h"
#include <string>

extern bead * beads[1000]; 
extern bond * bonds[1000];
extern int filament[1000][1000];
extern int N;
extern int L;
extern int Nbonds;
extern string name;
extern int TIME;
extern int mode;
extern double dt;
extern double length;
using namespace std;
using namespace Eigen;

void input(const std::string& file_name)
{

	double mass,size,xc,yc,zc;
	int idnum,number;
	string s,s1,s2,s3;
	ifstream file(file_name.c_str());
	//-----------------Loading beads----------------------------
	file >> s;
	name=s;
	file >> s;
        length=0.0;
	L = ::atoi(s.c_str());
	for (int i=0;i<L;i++){
		file >> s;	
		number=::atoi(s.c_str());
                N+=number;
                filament[i][0]=number;
		for (int y=0;y<number;y++) {
			file >> s1; idnum=atoi(s1.c_str());
			file >> s2; mass=atof(s2.c_str());
			file >> s3; size=atof(s3.c_str());
                        length+=size*2.0;
			//Creating a bead
			        Vector3f Position=Vector3f::Zero();
				Vector3f Velocity=Vector3f::Zero();
				Vector3f Field=Vector3f::Zero();
				Vector3f Force=Vector3f::Zero();
        			beads[idnum]= new bead(i,mass,size,Position);
                                filament[i][y+1]=idnum;
			//------------
			//Loading additional parameters for a bead
			file >> s1; xc=atof(s1.c_str());
			file >> s2; yc=atof(s2.c_str());
			file >> s3; zc=atof(s3.c_str());
			Position << xc, yc, zc; 
			file >> s1; xc=atof(s1.c_str());
			file >> s2; yc=atof(s2.c_str());
			file >> s3; zc=atof(s3.c_str());
			Velocity << xc,yc,zc;
			file >> s1; xc=atof(s1.c_str());
			file >> s2; yc=atof(s2.c_str());
			file >> s3; zc=atof(s3.c_str());
			Force << xc,yc,zc;
			file >> s1; xc=atof(s1.c_str());
			file >> s2; yc=atof(s2.c_str());
			file >> s3; zc=atof(s3.c_str());
			Field << xc,yc,zc;
			beads[idnum]->r=Position;
			beads[idnum]->v=Velocity;
			beads[idnum]->F=Force;
			beads[idnum]->u=Field;
			//---------------------------

		}
	
	}

	//---------------------------------------------------------
	//----------------------Loading bonds----------------------
	file >> s;  
	int ide1,ide2,ide3,ide4;	
	Nbonds = atoi(s.c_str());
	for (int i=0;i<Nbonds;i++){
			file >> s1; ide1=atoi(s1.c_str());
			file >> s2; ide2=atoi(s2.c_str());
			file >> s3; ide3=atoi(s3.c_str());
			file >> s ; ide4=atoi(s.c_str());
			bonds[ide1]= new bond(ide1,ide2,ide3,0,0,"");
			bonds[ide1]->id3=ide4;
			file >> s1; bonds[ide1]->nature=atoi(s1.c_str());
			file >> s2; bonds[ide1]->strength=atof(s2.c_str());
			file >> s3; bonds[ide1]->x0=atof(s3.c_str());
			file >>s; bonds[ide1]->name_bond=s;
																																										   	}
     //-----------------------------------------------------------	
	file.close();
}


void StoreXYZ(const std::string& extra) {


	ofstream coordinates_file;
	
 	string name_file="Coordinates/coordinates_";


	if (extra!="def")
	{
		name_file.append(extra);
		name_file.append(".xyz");
	}
	else
	{
		name_file.append(".xyz");
	}
	coordinates_file.open(name_file.c_str(),ios::app);


	//Writing
	coordinates_file << N << "\n" << "\n";
	for (int y=0;y<N;y++) {
		coordinates_file<< "P" << y << "  "<<beads[y]->r.transpose() << "\n";
	}
	//------

	coordinates_file.close();
	if (mode==2) {cout << "Data Saved in XYZ \n";}

}


void StoreDebugInfo(const std::string& extra, double precision, int t) {


	ofstream info_file;
	
 	string name_file="Info/info_";


	if (extra!="def")
	{
		name_file.append(extra);
		name_file.append(double_to_string(precision));
		name_file.append(".xyz");
	}
	else
	{
		name_file.append(".xyz");
		name_file.append(double_to_string(precision));
	}
	info_file.open(name_file.c_str(),ios::app);


	info_file << t*dt << " nanos ";
	for (int i=0;i<L;i++){
	info_file << " Filament " << i << " " << "Bulging: " << filamentbulgeY(i) << " Velocity: " << TotalVelocityOnFilament(i);
	info_file << " TotalForce " << TotalForceOnFilament(i) << " Gamma " << TotalMassOnFilament(i)*gravity/(TotalVelocityOnFilament(i));
	}
	info_file << " Distance Between Filaments: "<<distancebetweenfilaments(0,1) << "\n" ;

	info_file.close();
}


void StorePosition(const std::string& extra) {


        ofstream position_file;
        
        string name_file="Position/position_";


        if (extra!="def")
        {
                name_file.append(extra);
                name_file.append(".txt");
        }
        else
        {
                name_file.append(".txt");
        }
        position_file.open(name_file.c_str());



        for (int y=0;y<N;y++) {
                position_file << y << "  "<<beads[y]->r.transpose() << "\n";
        }
        //------

        position_file.close();
        if (mode==2  || mode == 7) {cout << "Data Saved in Position File \n";}

}


void StoreField(const std::string& extra) {


        ofstream field_file;
        
        string name_file="Field/field_";
        //usr/users/iff_th2/mutny/Desktop/Simulation_2/Find_vel_5/
        if (extra!="def")
        {
                name_file.append(extra);
                name_file.append(".txt");
        }
        else
        {
                name_file.append(".txt");
        }
        field_file.open(name_file.c_str(),ios::app);
        
        double x0=-5.0;
        double y0=-50.0;
        double z0=-100;
        double ze=0.0;
        double xe=55.0;
        double ye=40.0;


                
        x0= 1000000.0;
        y0= 1000000.0;
        z0= 1000000.0;
        xe=-1000000.0;
        ye=-1000000.0;
        ze=-1000000.0;
        
        
        //Finding the borders of the System
        for (int i=0;i<N;i++) {
                if (beads[i]->r(0)>xe) { xe=beads[i]->r(0); }
                if (beads[i]->r(0)<x0) { x0=beads[i]->r(0); }
                
                if (beads[i]->r(1)>ye) { ye=beads[i]->r(1); }
                if (beads[i]->r(1)<y0) { y0=beads[i]->r(1); }

                if (beads[i]->r(2)>ze) { ze=beads[i]->r(2); }
                if (beads[i]->r(2)<z0) { z0=beads[i]->r(2); }
        }



        //-----------------------------------
        double step=0.1 ;
        double x,y,z;

        Vector3f Position=Vector3f::Zero();
        beads[N+1]=new bead(N,beads[0]->m,beads[0]->size,Position);
        x=x0;
        z=z0;
        while (x<xe) {
                y=y0;
                while (y<ye) {
                        //for (int z=z0;z<ze;z+step) {
                                Position << x,y,z;
                                beads[N+1]->r=Position;
                                beads[N+1]->calculate_flow_field_from_force(0);
                                field_file << x << " " << y << " "  << beads[N+1]->u.transpose()*(1e4) << "\n";  
      
                        //}
                y=y+step;
                }
                x=x+step;
        }
        

        
       field_file.close();
        if (mode==2 || mode==7) {cout << "Field Data Saved \n";}
        

}


void StoreDebug(const std::string& extra,double time_of_simulation) {

	ofstream debug;
	string name_file="Debug/debug_";
	
	if (extra!="def")
	{
		name_file.append(extra);
		name_file.append(".txt");
	}
	else
	{
		name_file.append(".txt");
	}
	debug.open(name_file.c_str(),ios::app);

	//---------
	debug << "-------------------------------------------" <<  time_of_simulation<< "--------------------------------\n";
	for (int y=0;y<N;y++) {
		debug << "P " << y ;
		debug << " Position " << beads[y]->r.transpose() ;
		debug << " Velocity " << beads[y]->v.transpose() ;
		debug << " Force " << beads[y]->F.transpose() << std::endl;
	}
	if (mode==3) {
		cout << "-------------------------------------------" <<  time_of_simulation<< "--------------------------------\n";
		for (int y=0;y<N;y++) {
			cout << "P " << y ;
			cout << " Position " << beads[y]->r.transpose() ;
			cout << " Velocity " << beads[y]->v.transpose() ;
			cout << " Mass " << beads[y]->m;
			cout << " Size " << beads[y]->size;
			cout << " Force " << beads[y]->F.transpose() << std::endl;
		}
	}

	
	//-----------
	debug.close();
	if (mode==2) {	cout << "Data Saved in Debug \n";}

}


void Debugout(const std::string& extra,double time_of_simulation) {

        if (mode==3) {
                cout << "-------------------------------------------" <<  time_of_simulation<< "--------------------------------\n";
                for (int y=0;y<N;y++) {
                        cout << "P " << y ;
                        cout << " Position " << beads[y]->r.transpose() ;
                        cout << " Velocity " << beads[y]->v.transpose() ;
                        cout << " Mass " << beads[y]->m;
                        cout << " Size " << beads[y]->size;
                        cout << " Force " << beads[y]->F.transpose() << std::endl;
                }
        }

}

void Initialize_IO(const std::string& in) {
	

	if (mode==1 || mode==3) {
		cout << "--------------------------------\n";
		cout << "1. Inititializing file loaded:";
	}

	input(in);
	if (mode==1 || mode==3 ) { cout << " Done\n"; }

	string name_file1="Debug/debug_";
	string name_file2="Coordinates/coordinates_";
	string name_file3="Field/field_";
        string name_file4="Position/position_";        

	if (name!="def")
	{
		name_file1.append(name);
		name_file1.append(".txt");
		name_file2.append(name);
		name_file2.append(".xyz");
                name_file3.append(name);
                name_file3.append(".txt");
                name_file4.append(name);
                name_file4.append(".txt");
	}
	else
	{
		name_file1.append(".txt");
		name_file2.append(".xyz");
                name_file3.append(".txt");
                name_file4.append(".txt");
	}
	
	
	remove(name_file1.c_str());
	remove(name_file2.c_str());
	remove(name_file3.c_str());	
        remove(name_file4.c_str());

	if (mode==1 || mode==3 ) {
		cout << "2.  Removed previously existing files (if have)\n";
		cout << "3.  Debug File recreated, name: " << name_file1 << "\n";
		cout << "4.  XYZ File recreated, name:  " << name_file2 << "\n";
                cout << "5.  Fluid Field File recreated, name: " << name_file3 << "\n";
                cout << "6.  Last Position File recreated, name: " << name_file4 << "\n";
		cout << "7.  Name of the simulation: "<< name << "\n";
		cout << "--------------------------------\n";
		cout << "8.  Filaments: " << L << "\n";
		cout << "9.  Beads: " << N <<"\n";
		cout << "10. Bonds: " << Nbonds << "\n";
		cout << "--------------------------------\n";
	}


}





