# vmd tcl procedure: reload the last trajectory data set 
#                    for the current top molecule. 
#
# $Id$
#
# Copyright (c) 2004 by <Axel.Kohlmeyer@theochem.ruhr-uni-bochum.de>
#
# TODO: - add some flags to handle more than one molecule
#       - store the last data set index somewhere globally,
#         so that this would also work for multple files
#       - support arguments for selecting molecules and
#         trajectory data files.
#       - support passing the 'waitfor' flag to mol addfile.

proc reload {args} {
    set viewpoints {}
    set mol [molinfo top]

    # save orientation and zoom parameters
    set viewpoints [molinfo $mol get { center_matrix rotate_matrix scale_matrix global_matrix }]
    
    # delete all coordinates and (re)load the latest data set.
    animate delete all
    set files [lindex [molinfo $mol get filename] 0]
    set lf [expr [llength $files] - 1]

    mol addfile [lindex $files $lf] \
        type [lindex [lindex [molinfo $mol get filetype] 0] $lf]

    # restore orientation and zoom
    foreach mol [molinfo list] {
	molinfo $mol set { center_matrix rotate_matrix scale_matrix global_matrix } $viewpoints
    }

    # clean up
    unset viewpoints
    unset mol
    unset files
    unset lf
}

